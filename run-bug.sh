#/bin/bash

set -xe

echo "creating circular link"

mkdir -p mydir/foo
ln -s  ../../ mydir/foo/back_to_root

echo "Creating artifacts"

echo "one" > one.bin
echo "two" > two.bin
echo "three" > three.bin

echo "Done"
